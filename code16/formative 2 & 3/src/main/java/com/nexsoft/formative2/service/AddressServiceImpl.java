package com.nexsoft.formative2.service;

import java.util.List;


import com.nexsoft.formative2.dao.AddressDaoImpl;
import com.nexsoft.formative2.entities.Address;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    AddressDaoImpl addressDaoImpl;

    @Override
    public void addAddress(Address address) {
        addressDaoImpl.addAddress(address);
    }

    @Override
    public List<Address> getAllAddress() {
        return addressDaoImpl.getAllAddress();
    }

    @Override
    public Address findAddressById(int id) {
        return addressDaoImpl.findAddressById(id);
    }

 

    @Override
    public void updateAddress(Address Address) {
        addressDaoImpl.updateAddress(Address);
    }

    @Override
    public void deleteAddress(int id) {
        addressDaoImpl.deleteAddress(id);
    }

}
