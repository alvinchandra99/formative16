package com.nexsoft.formative2.service;

import java.util.List;

import com.nexsoft.formative2.entities.Address;

public interface AddressService {

    

    public List<Address> getAllAddress();

    public Address findAddressById(int id);

    public void addAddress(Address employee);

    public void updateAddress(Address employee);

    public void deleteAddress(int id);
}
