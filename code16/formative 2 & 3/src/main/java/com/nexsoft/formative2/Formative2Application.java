package com.nexsoft.formative2;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Formative2Application {

	public static void main(String[] args) {
		SpringApplication.run(Formative2Application.class, args);
	}

}
