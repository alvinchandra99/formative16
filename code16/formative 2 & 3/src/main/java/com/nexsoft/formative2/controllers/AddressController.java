package com.nexsoft.formative2.controllers;

import java.util.List;

import com.nexsoft.formative2.entities.Address;
import com.nexsoft.formative2.service.AddressServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AddressController {

    @Autowired
    AddressServiceImpl addressServiceImpl;

    @GetMapping("/listaddress")
    public List<Address> getAllAddress() {
        return addressServiceImpl.getAllAddress();
    }

    public void updateAddres(Address address) {
        addressServiceImpl.updateAddress(address);
    }

    public void deleteAddress(int id) {
        addressServiceImpl.deleteAddress(id);
    }

    public Address findAddressById(int id) {
        return addressServiceImpl.findAddressById(id);
    }

}
