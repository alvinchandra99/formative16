package com.nexsoft.formative2.entities;

public class Address {
    
    public Address(int id, String receipent, String gender, String street, int zipCode, String district, String city,
            String province, String country, String assurance, String description, String totalPrice,
            String deliveryCost) {
        this.id = id;
        this.receipent = receipent;
        this.gender = gender;
        this.street = street;
        this.zipCode = zipCode;
        this.district = district;
        this.city = city;
        this.province = province;
        this.country = country;
        this.assurance = assurance;
        this.description = description;
        this.totalPrice = totalPrice;
        this.deliveryCost = deliveryCost;
    }

    public String getTotalPrice() {
        return totalPrice;
    }



    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    private int id;

    private String receipent;

    private String gender;

    private String street;

    private int zipCode;

    private String district;

    private String city;

    private String province;

    private String country;

    private String assurance;

    private String description;

    private String totalPrice;

    private String deliveryCost;

    public Address() {
    }

    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getReceipent() {
        return receipent;
    }

    public void setReceipent(String receipent) {
        this.receipent = receipent;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public int getZipCode() {
        return zipCode;
    }

    public void setZipCode(int zipCode) {
        this.zipCode = zipCode;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }



    public String getAssurance() {
        return assurance;
    }



    public void setAssurance(String assurance) {
        this.assurance = assurance;
    }



    public String getDescription() {
        return description;
    }



    public void setDescription(String description) {
        this.description = description;
    }



    public String getDeliveryCost() {
        return deliveryCost;
    }



    public void setDeliveryCost(String deliveryCost) {
        this.deliveryCost = deliveryCost;
    }

    

}