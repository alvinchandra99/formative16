package com.nexsoft.formative2.rowmapper;

import com.nexsoft.formative2.entities.Address;
import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;

public class AddressRowMapper implements RowMapper<Address> {

    @Override
    public Address mapRow(ResultSet rs, int rowNum) throws SQLException {
        Address address = new Address();
        address.setId(rs.getInt("id"));
        address.setReceipent(rs.getString("receipent"));
        address.setGender(rs.getString("gender"));
        address.setStreet(rs.getString("street"));
        address.setZipCode(rs.getInt("zipCode"));
        address.setDistrict(rs.getString("district"));
        address.setCity(rs.getString("city"));
        address.setProvince(rs.getString("province"));
        address.setCountry(rs.getString("country"));
        return address;
    }
}
