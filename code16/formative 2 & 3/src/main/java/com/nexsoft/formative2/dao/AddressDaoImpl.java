package com.nexsoft.formative2.dao;

import java.util.List;

import com.nexsoft.formative2.entities.Address;
import com.nexsoft.formative2.rowmapper.AddressRowMapper;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Repository
public class AddressDaoImpl implements AddressDao {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Override
    public void addAddress(Address address) {
        String query = "INSERT INTO address(id, receipent, gender, street, zipCode, district, city, province, country, assurance, totalPrice, deliveryCost) VALUES(?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?)";
        jdbcTemplate.update(query, address.getId(), address.getReceipent(), address.getGender(), address.getStreet(),
                address.getZipCode(), address.getDistrict(), address.getCity(), address.getProvince(),
                address.getCountry(), address.getAssurance(), address.getTotalPrice(), address.getDeliveryCost());

    }

    @Override
    public List<Address> getAllAddress() {
        String query = "SELECT * from address";
        RowMapper<Address> rowMapper = new AddressRowMapper();
        List<Address> list = jdbcTemplate.query(query, rowMapper);

        return list;
    }

    @Override
    public Address findAddressById(int id) {
        String query = "SELECT * FROM address WHERE id = ?";
        RowMapper<Address> rowMapper = new BeanPropertyRowMapper<Address>(Address.class);
        Address address = jdbcTemplate.queryForObject(query, rowMapper, id);

        return address;
    }

    @Override
    public void updateAddress(Address address) {
        String query = "UPDATE employees SET receipent=?, gender=?, street=?, zipCode=?, district=?, city=?, province=?, country=?, assurance=?, totalPrice=?, deliveryCos=? WHERE id=?";
        jdbcTemplate.update(query, address.getReceipent(), address.getGender(), address.getStreet(),
                address.getZipCode(), address.getDistrict(), address.getCity(), address.getProvince(),
                address.getCountry(), address.getAssurance(), address.getTotalPrice(), address.getDeliveryCost(),
                address.getId());

    }

    @Override
    public void deleteAddress(int id) {
        String query = "DELETE FROM address WHERE id=?";
        jdbcTemplate.update(query, id);
    }

}