package com.nexsoft.formative2.dao;

import java.util.List;

import com.nexsoft.formative2.entities.Address;

public interface AddressDao {

    public void addAddress(Address address);

    public List<Address> getAllAddress();
 
    public Address findAddressById(int id);
    
    public void updateAddress(Address address);
    
    public void deleteAddress(int id);

}