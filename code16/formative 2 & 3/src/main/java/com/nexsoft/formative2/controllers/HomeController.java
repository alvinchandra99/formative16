package com.nexsoft.formative2.controllers;

import com.nexsoft.formative2.entities.Address;
import com.nexsoft.formative2.service.AddressServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

    @Autowired
    AddressServiceImpl addressServiceImpl;

    @GetMapping("/")
    public ModelAndView home() {
        ModelAndView mv = new ModelAndView("index");
        return mv;
    }

    @PostMapping("/save")
    public ModelAndView save(Address address) {

        addressServiceImpl.addAddress(address);

        return new ModelAndView("redirect:/");
    }

}