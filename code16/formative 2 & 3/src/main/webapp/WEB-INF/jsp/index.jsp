<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Bootstrap CSS -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x"
      crossorigin="anonymous"
    />

    <title>Website</title>
  </head>
  <body>

  <form action="/save" method="post">
    <div class="container mt-5" style="max-width:800px;">
      <h3 class="mb-5" style="text-align: center">Sending Address</h3>

      <!-- Receipent-->
      <div class="mb-3">
        <div class="row">
          <div class="col-6">
            <label class="form-label">Receipent</label>
          </div>

          <div class="col-6">
            <input type="text" name="receipent" class="form-control" />
          </div>
        </div>
      </div>

      <!--Gender-->

      <div class="mb-3">
        <div class="row">
          <div class="col-6">
            <label class="form-label">Gender</label>
          </div>

          <div class="col-6">
            <input class="form-check-input" name="gender" value="male" type="radio" />
            <label class="form-check-label"> Male </label>

            <input class="form-check-input" name="gender" value="female" type="radio" />
            <label class="form-check-label"> Female </label>
          </div>
        </div>
      </div>

      <!-- Street-->
      <div class="mb-3">
        <div class="row">
          <div class="col-6">
            <label class="form-label">Street</label>
          </div>

          <div class="col-6">
            <input type="text" name="street" class="form-control" />
          </div>
        </div>
      </div>

      <!-- Zip Code-->
      <div class="mb-3">
        <div class="row">
          <div class="col-6">
            <label class="form-label">Zip Code</label>
          </div>

          <div class="col-6">
            <input type="text" name="zipCode" class="form-control" />
          </div>
        </div>
      </div>

      <!-- District-->
      <div class="mb-3">
        <div class="row">
          <div class="col-6">
            <label class="form-label">District</label>
          </div>

          <div class="col-6">
            <input type="text" name="district" class="form-control" />
          </div>
        </div>
      </div>

      <!-- City-->
      <div class="mb-3">
        <div class="row">
          <div class="col-6">
            <label class="form-label">City</label>
          </div>

          <div class="col-6">
            <input type="text" name="city" class="form-control" />
          </div>
        </div>
      </div>

      <!-- Province-->
      <div class="mb-3">
        <div class="row">
          <div class="col-6">
            <label class="form-label">Province</label>
          </div>

          <div class="col-6">
            <input type="text" name="province" class="form-control" />
          </div>
        </div>
      </div>

      <!-- Country-->
      <div class="mb-3">
        <div class="row">
          <div class="col-6">
            <label for="exampleFormControlInput1" class="form-label"
              >Country</label
            >
          </div>

          <div class="col-6">
            <select class="form-select" name="country" aria-label="Default select example">
              <option selected>Please Select</option>
              <option value="indonesia">Indonesia</option>
              <option value="malaysia">Malaysia</option>
              <option value="singapore">Singapore</option>
            </select>
          </div>
        </div>
      </div>

      <!-- Assurance-->
      <div class="mb-3">
        <div class="row">
          <div class="col-6">
            <label for="exampleFormControlInput1" class="form-label"
              >Assurance (10%)</label
            >
          </div>

          <div class="col-6">
            <input class="form-check-input" type="checkbox" id="assurance" name = "assurance" />
            <label class="form-check-label" for="flexCheckDefault">
              Checkbox
            </label>
          </div>
        </div>
      </div>

      <!-- Textbox-->
      <div class="mb-3">
        <div class="row">
          <div class="col-6">
            <label class="form-label"></label>
          </div>

          <div class="col-6">
            <label class="form-label">Example textarea</label>
            <textarea class="form-control" rows="6" name="description" ></textarea>
          </div>
        </div>
      </div>

      <!-- Total Price-->
      <div class="mb-3">
        <div class="row">
          <div class="col-6">
            <label class="form-label">Total Price</label>
          </div>

          <div class="col-6">
            <input type="number" number="totalPrice" class="form-control" />
          </div>
        </div>
      </div>

      <!-- Delivery Servie Cost-->
      <div class="mb-5">
        <div class="row">
          <div class="col-6">
            <label class="form-label">Delivery Service Cost</label>
          </div>

          <div class="col-6">
            <input name="deliveryCost" type="number" class="form-control" />
          </div>
        </div>
      </div>
      <div class="mb-5 float-end">
            <button type="submit" class="btn btn-success btn-lg">Send</button>
      </div>


    </div>
    </form>

    <!-- Optional JavaScript; choose one of the two! -->
    <script>
    assurance =document.getElemetById("assurance");
    if(assurance.checked){
        assurance.value = true;
    } 
    assurance.value = false;
    </script>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4"
      crossorigin="anonymous"
    ></script>
  </body>
</html>
